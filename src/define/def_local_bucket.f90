!> ローカルバケットセルモジュール
module mod_ggtools_def_local_bucket
  use mod_monolis_utils
  implicit none

  real(kdouble) :: ths = 1.0d-6

  !> @ingroup bucket
  !> 第 i 番目のバケットセルの基本情報構造体
  type type_ggtools_local_bucket_cells
    !> バケットセルに登録された個数
    integer(kint) :: nid = 0
    !> バケットセルに登録された nid 個の 1 次元整数配列
    integer(kint), allocatable :: id(:)
  end type type_ggtools_local_bucket_cells

  !> @ingroup bucket
  !> バケットの基本情報構造体
  type type_ggtools_local_bucket
    !> バウンダリボックスの最小座標
    real(kdouble) :: xmin(3)
    !> バウンダリボックスの最大座標
    real(kdouble) :: xmax(3)
    !> バケットセル分割数（nx, ny, nz）
    integer(kint) :: nx(3)
    !> バケットセルひとつのサイズ（dx, dy, dz）
    real(kdouble) :: dx(3)
  end type type_ggtools_local_bucket

contains

  !> @ingroup bucket
  !> バケットの初期化処理（バケットセルサイズを入力）
  !> @details バウンダリボックスの最小座標 xmin を起点に、バウンディングボックスを覆う最小のバケットセルを確保する。
  !> @details バケットセル確保後に、バウンダリボックスの最大座標 xmax が更新される。
  subroutine ggtools_local_bucket_init(ggtools_bucket, xmin, xmax, dx)
    implicit none
    !> バケット構造体
    type(type_ggtools_local_bucket) :: ggtools_bucket
    !> バウンダリボックスの最小座標
    real(kdouble) :: xmin(3)
    !> バウンダリボックスの最大座標
    real(kdouble) :: xmax(3)
    !> バケットセルサイズ（dx, dy, dz）
    real(kdouble) :: dx(3)
    integer(kint) :: nx(3)

    ggtools_bucket%xmin = xmin
    ggtools_bucket%xmax = xmax

    ggtools_bucket%dx = dx

    nx(1) = ceiling( (xmax(1) - xmin(1))/dx(1) )
    nx(2) = ceiling( (xmax(2) - xmin(2))/dx(2) )
    nx(3) = ceiling( (xmax(3) - xmin(3))/dx(3) )

    if(nx(1) < 1 .or. nx(2) < 1 .or. nx(3) < 1)then
      call monolis_std_error_string("the number of local bucket cells is less than 1")
      call monolis_std_error_stop()
    endif

    xmax(1) = dx(1)*nx(1)
    xmax(2) = dx(2)*nx(2)
    xmax(3) = dx(3)*nx(3)

    ggtools_bucket%nx(1) = nx(1)
    ggtools_bucket%nx(2) = nx(2)
    ggtools_bucket%nx(3) = nx(3)
  end subroutine ggtools_local_bucket_init

  !> @ingroup bucket
  !> バケットの終了処理
  subroutine ggtools_local_bucket_finalize(ggtools_bucket)
    implicit none
    !> バケット構造体
    type(type_ggtools_local_bucket) :: ggtools_bucket

    ggtools_bucket%xmin = 0.0d0
    ggtools_bucket%xmax = 0.0d0
    ggtools_bucket%dx = 0.0d0
    ggtools_bucket%nx = 0
  end subroutine ggtools_local_bucket_finalize

  !> @ingroup bucket
  !> バケットセルの初期化処理
  subroutine ggtools_local_bucket_cells_init(ggtools_bucket_cell, nx)
    implicit none
    !> バケットセル構造体
    type(type_ggtools_local_bucket_cells), allocatable :: ggtools_bucket_cell(:)
    !> バケットセル分割数（nx, ny, nz）
    integer(kint) :: nx(3)
    integer(kint) :: n_total

    n_total = nx(1)*nx(2)*nx(3)
    allocate(ggtools_bucket_cell(n_total))
  end subroutine ggtools_local_bucket_cells_init

  !> @ingroup bucket
  !> バケットセルの終了処理
  subroutine ggtools_local_bucket_cells_finalize(ggtools_bucket_cell, nx)
    implicit none
    !> バケットセル構造体
    type(type_ggtools_local_bucket_cells) :: ggtools_bucket_cell(:)
    !> バケットセル分割数（nx, ny, nz）
    integer(kint) :: nx(3)
    integer(kint) :: i, n_total

    n_total = nx(1)*nx(2)*nx(3)

    do i = 1, n_total
      if(ggtools_bucket_cell(i)%nid == 0) cycle
      deallocate(ggtools_bucket_cell(i)%id)
    enddo
  end subroutine ggtools_local_bucket_cells_finalize

  !> @ingroup bucket
  !> バケットサイズを取得
  subroutine ggtools_local_bucket_get_bucket_size(ggtools_bucket, xmin, xmax)
    implicit none
    !> バケット構造体
    type(type_ggtools_local_bucket) :: ggtools_bucket
    !> バウンダリボックスの最小座標
    real(kdouble) :: xmin(3)
    !> バウンダリボックスの最大座標
    real(kdouble) :: xmax(3)
    xmin = ggtools_bucket%xmin
    xmax = ggtools_bucket%xmax
  end subroutine ggtools_local_bucket_get_bucket_size

  !> @ingroup bucket
  !> バケットセル分割数を取得
  subroutine ggtools_local_bucket_get_number_of_bucket_divisions(ggtools_bucket, nx)
    implicit none
    !> バケット構造体
    type(type_ggtools_local_bucket) :: ggtools_bucket
    !> バケットセル分割数（nx, ny, nz）
    integer(kint) :: nx(3)
    nx = ggtools_bucket%nx
  end subroutine ggtools_local_bucket_get_number_of_bucket_divisions

  !> @ingroup bucket
  !> バケットセルサイズを取得
  subroutine ggtools_local_bucket_get_size_of_one_bucket_cell(ggtools_bucket, dx)
    implicit none
    !> バケット構造体
    type(type_ggtools_local_bucket) :: ggtools_bucket
    !> バケットセルサイズ（dx, dy, dz）
    real(kdouble) :: dx(3)
    dx = ggtools_bucket%dx
  end subroutine ggtools_local_bucket_get_size_of_one_bucket_cell
end module mod_ggtools_def_local_bucket
