!> ローカルバケットセルモジュール
module mod_ggtools_local_bucket
  use mod_monolis_utils
  use mod_ggtools_def_local_bucket
  use mod_ggtools_bucket_util
  implicit none

contains

  !> @ingroup bucket
  !> LBB を指定してバケットセルに id を登録する関数
  subroutine ggtools_local_bucket_set_id_by_lbb(ggtools_bucket, ggtools_bucket_cell, xmin_local, xmax_local, id)
    implicit none
    !> バケット構造体
    type(type_ggtools_local_bucket) :: ggtools_bucket
    !> バケットセル構造体
    type(type_ggtools_local_bucket_cells) :: ggtools_bucket_cell(:)
    !> ローカルバウンダリボックス LBB の最小座標
    real(kdouble) :: xmin_local(3)
    !> ローカルバウンダリボックス LBB の最大座標
    real(kdouble) :: xmax_local(3)
    !> バケットセルに登録する id
    integer(kint) :: id, x, y, z
    integer(kint) :: int_id(3), in, imin(3), imax(3)
    real(kdouble) :: x_point(3)

    x_point(1) = xmin_local(1) - ths
    x_point(2) = xmin_local(2) - ths
    x_point(3) = xmin_local(3) - ths
    imin = ggtools_bucket_get_integer_coordinate_from_real_coordinate(ggtools_bucket%xmin, ggtools_bucket%dx, x_point)

    x_point(1) = xmax_local(1) + ths
    x_point(2) = xmax_local(2) + ths
    x_point(3) = xmax_local(3) + ths
    imax = ggtools_bucket_get_integer_coordinate_from_real_coordinate(ggtools_bucket%xmin, ggtools_bucket%dx, x_point)

    do z = imin(3), imax(3)
    do y = imin(2), imax(2)
    do x = imin(1), imax(1)
      int_id(1) = x
      int_id(2) = y
      int_id(3) = z
      in = ggtools_bucket_get_1d_array_index_from_integer_coodinate(ggtools_bucket%nx, int_id)
      call ggtools_bucket_set_id_main(ggtools_bucket_cell(in), id)
    enddo
    enddo
    enddo
  end subroutine ggtools_local_bucket_set_id_by_lbb

  !> @ingroup bucket
  !> 座標を指定して 1 つのバケットセルに id を登録する関数
  subroutine ggtools_local_bucket_set_id_by_point(ggtools_bucket, ggtools_bucket_cell, x_point, id)
    implicit none
    !> バケット構造体
    type(type_ggtools_local_bucket) :: ggtools_bucket
    !> バケットセル構造体
    type(type_ggtools_local_bucket_cells) :: ggtools_bucket_cell(:)
    !> バゲットセルを指定する座標
    real(kdouble) :: x_point(3)
    !> バケットセルに登録する id
    integer(kint) :: id
    integer(kint) :: in
    integer(kint) :: int_id(3)

    int_id = ggtools_bucket_get_integer_coordinate_from_real_coordinate(ggtools_bucket%xmin, ggtools_bucket%dx, x_point)
    in = ggtools_bucket_get_1d_array_index_from_integer_coodinate(ggtools_bucket%nx, int_id)
    call ggtools_bucket_set_id_main(ggtools_bucket_cell(in), id)
  end subroutine ggtools_local_bucket_set_id_by_point

  !> @ingroup dev
  !> バケットへの情報登録（メイン関数）
  subroutine ggtools_local_bucket_set_id_main(ggtools_bucket_cell, data)
    implicit none
    !> バケット検索構造体
    type(type_ggtools_local_bucket_cells) :: ggtools_bucket_cell
    !> 登録する要素領域 id
    integer(kint) :: data
    integer(kint) :: add(1)

    add = data
    call monolis_append_I_1d(ggtools_bucket_cell%id, 1, add)
    ggtools_bucket_cell%nid = ggtools_bucket_cell%nid + 1
  end subroutine ggtools_local_bucket_set_id_main

  !> @ingroup bucket
  !> 入力した座標を内包するバケットセルから、nid 個の整数配列 id_array を取得する関数
  subroutine ggtools_local_bucket_get_id_by_point(ggtools_bucket, ggtools_bucket_cell, x_point, nid, id_array)
    implicit none
    !> バケット構造体
    type(type_ggtools_local_bucket) :: ggtools_bucket
    !> バケットセル構造体
    type(type_ggtools_local_bucket_cells) :: ggtools_bucket_cell(:)
    !> バゲットセルを指定する座標
    real(kdouble) :: x_point(3)
    !> バケットセルに登録された個数
    integer(kint) :: nid
    !> バケットセルに登録された nid 個の 1 次元整数配列
    integer(kint), allocatable :: id_array(:)
    integer(kint) :: int_id(3), index

    int_id = ggtools_bucket_get_integer_coordinate_from_real_coordinate(ggtools_bucket%xmin, ggtools_bucket%dx, x_point)
    index = ggtools_bucket_get_1d_array_index_from_integer_coodinate(ggtools_bucket%nx, int_id)
    call ggtools_bucket_get_id_by_1d_array_index(ggtools_bucket_cell, index, nid, id_array)
  end subroutine ggtools_local_bucket_get_id_by_point

  !> @ingroup bucket
  !> 入力した LBB を内包するバケットセルから、nid 個の整数配列 id_array を取得する関数
  subroutine ggtools_local_bucket_get_id_by_lbb(ggtools_bucket, ggtools_bucket_cell, xmin_local, xmax_local, nid, id_array)
    implicit none
    !> バケット構造体
    type(type_ggtools_local_bucket) :: ggtools_bucket
    !> バケットセル構造体
    type(type_ggtools_local_bucket_cells) :: ggtools_bucket_cell(:)
    !> ローカルバウンダリボックス LBB の最小座標
    real(kdouble) :: xmin_local(3)
    !> ローカルバウンダリボックス LBB の最大座標
    real(kdouble) :: xmax_local(3)
    !> バケットセルに登録された個数
    integer(kint) :: nid
    !> バケットセルに登録された nid 個の 1 次元整数配列
    integer(kint), allocatable :: id_array(:)
    integer(kint) :: x, y, z, int_id(3), imin(3), imax(3), nid_temp, nid_all, index
    real(kdouble) :: x_point(3)
    integer(kint), allocatable :: id_array_temp(:), id_array_all(:)

    x_point(1) = xmin_local(1) - ths
    x_point(2) = xmin_local(2) - ths
    x_point(3) = xmin_local(3) - ths
    imin = ggtools_bucket_get_integer_coordinate_from_real_coordinate(ggtools_bucket%xmin, ggtools_bucket%dx, x_point)

    x_point(1) = xmax_local(1) + ths
    x_point(2) = xmax_local(2) + ths
    x_point(3) = xmax_local(3) + ths
    imax = ggtools_bucket_get_integer_coordinate_from_real_coordinate(ggtools_bucket%xmin, ggtools_bucket%dx, x_point)

    nid_all = 0

    do z = imin(3), imax(3)
    do y = imin(2), imax(2)
    do x = imin(1), imax(1)
      int_id(1) = x
      int_id(2) = y
      int_id(3) = z
      index = ggtools_bucket_get_1d_array_index_from_integer_coodinate(ggtools_bucket%nx, int_id)
      call ggtools_bucket_get_id_by_1d_array_index(ggtools_bucket_cell, index, nid_temp, id_array_temp)
      call monolis_append_I_1d(id_array_all, nid_temp, id_array_temp)
      nid_all = nid_all + nid_temp
    enddo
    enddo
    enddo

    if(nid_all == 0)then
      nid = 0
      return
    endif

    call monolis_qsort_I_1d(id_array_all, 1, nid_all)
    call monolis_get_uniq_array_I(id_array_all, nid_all, nid)

    call monolis_alloc_I_1d(id_array, nid)
    id_array = id_array_all(1:nid)
  end subroutine ggtools_local_bucket_get_id_by_lbb

  !> @ingroup bucket
  !> 入力した座標を内包するバケットセルから、nid 個の整数配列 id_array を取得する関数
  subroutine ggtools_local_bucket_get_id_by_1d_array_index(ggtools_bucket_cell, index, nid, id_array)
    implicit none
    !> バケットセル構造体
    type(type_ggtools_local_bucket_cells) :: ggtools_bucket_cell(:)
    !> バケットセルの 1 次元配列通し番号
    integer(kint) :: index
    !> バケットセルに登録された個数
    integer(kint) :: nid
    !> バケットセルに登録された nid 個の 1 次元整数配列
    integer(kint), allocatable :: id_array(:)

    if(ggtools_bucket_cell(index)%nid == 0)then
      nid = 0
      return
    endif

    nid = ggtools_bucket_cell(index)%nid
    call monolis_alloc_I_1d(id_array, nid)
    id_array = ggtools_bucket_cell(index)%id
  end subroutine ggtools_local_bucket_get_id_by_1d_array_index

end module mod_ggtools_local_bucket
